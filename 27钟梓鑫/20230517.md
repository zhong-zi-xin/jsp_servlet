1. MySQL中创建一个数据库student_db

2. 库中创建student表

3. 表中数据如下

4. | 编号 | 姓名 | 性别 |
   | ---- | ---- | ---- |
   | 1    | 张三 | 男   |
   | 2    | 李四 | 女   |
   | 3    | 王五 | 男   |

   ~~~MySQL
   create database student_db default char set utf8;
   use student_db;
   create table student_db  (
       id int ,
       name varchar(10),
       sex char
       );
   insert into student_db values
       (1,'张三','男'),
       (2,'李四','女'),
       (3,'王五','男');


扩展题【预习题】

能否实现一个类中，用四个方法来实现上面4个类的功能
能否实现将查询的结果，封装成java对象
import java.sql.*;

public class Jdbc {
    public static void main(String[] args) {
        try {
            select();//查询
            update();//修改
            insert();//添加
            delete();//删除
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }
    public static void select() throws SQLException, ClassNotFoundException {
        //1.注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //2.获取连接对象
        String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
        String username = "root";
        String password = "root";
        Connection conn = DriverManager.getConnection(url,username,password);
        //3.定义SQL
        String sql = "select * from sonyang;";

        //4.获取执行SQL
        Statement st = conn.createStatement();

        //5.执行SQL
        ResultSet rs = st.executeQuery(sql);
        //6.处理返回结果
        System.out.println("id      "+"name");
        while (rs.next()){
            int id = rs.getInt("id");
            String name  = rs.getString("name");
            System.out.println(id+"       "+name);
        }
        //7.释放资源
        rs.close();
        st.close();
        conn.close();
    }
    public static void update() throws SQLException, ClassNotFoundException {
        //1.注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //2.获取连接对象
        String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
        String username = "root";
        String password = "root";
        Connection conn = DriverManager.getConnection(url,username,password);
        //3.定义SQL
        String sql = "update sonyang set name = '宋扬2号'where id=1";

        //4.获取执行SQL
        Statement st = conn.createStatement();

        //5.执行SQL
        int i = st.executeUpdate(sql);
        //6.处理返回结果
       if (i>0){
           System.out.println("修改成功");
       }else {
           System.out.println("修改失败");
       }
        //7.释放资源
        st.close();
        conn.close();
    }
    public static void insert() throws ClassNotFoundException, SQLException {
        //1.注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //2.获取连接对象
        String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
        String username = "root";
        String password = "root";
        Connection conn = DriverManager.getConnection(url,username,password);
        //3.定义SQL
        String sql = "insert into sonyang values (2,'邓琪plus')";
        //4.获取执行SQL
        Statement st = conn.createStatement();

        //5.执行SQL
        int i = st.executeUpdate(sql);
        //6.处理返回结果
        if (i>0){
            System.out.println("添加"+i+"行数据成功");
        }else {
            System.out.println("添加失败");
        }
        //7.释放资源
        st.close();
        conn.close();
    }
    public static void delete() throws ClassNotFoundException, SQLException {
        //1.注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //2.获取连接对象
        String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
        String username = "root";
        String password = "root";
        Connection conn = DriverManager.getConnection(url,username,password);
        //3.定义SQL
        String sql = "delete from sonyang where name='张三'";
        //4.获取执行SQL
        Statement st = conn.createStatement();
        //5.执行SQL
        int i = st.executeUpdate(sql);
        //6.处理返回结果
        if (i>0){
            System.out.println("删除了"+i+"行数据成功");
        }else {
            System.out.println("删除失败");
        }
        //7.释放资源
        st.close();
        conn.close();

    }
}